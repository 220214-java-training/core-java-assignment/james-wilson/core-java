/*******************************************************************
 James Wilson
 Assignment #1
 Monday, 28 February 2022
 Instructor: Carolyn Rehm

 Driver: EvaluationServiceTest.java
 Source: EvaluationService.java

 In this module, ten (10) programming problems are solved using Java as the high-level source language!!!

 Each of the ten problem descriptions is described in a heading directly before the source code for each problem.

 Program execution is accomplished through the use of the driver file!

 The entire file (all ten problems) can be run as a whole by choosing the green arrow next to class file.

 Individual problems can be run by choosing the green arrow next to each problem heading.
 ******************************************************************/

// Driver File: EvaluationServiceTest.java
// Source Code File: (below)

package com.revature.eval.java.core;

import org.jetbrains.annotations.NotNull;

import java.time.temporal.Temporal;
import java.util.List;
import java.lang.Object;
import java.util.Map;
import java.util.Arrays;

/**
 * 1. Without using the StringBuilder or StringBuffer class, write a method that
 * reverses a String. Example: reverse("example"); -> "elpmaxe"
 *
 */

public class EvaluationService {
	public String reverse(@NotNull String string) {
		// TODO Write an implementation for this method declaration
		// declare an empty string to store the reversed letters of the string
		String result = "";
		// for loop starts at the end of the string and iterates to index 0
		for (int i = string.length() - 1; i >= 0; i--) {
			//result = result + string.charAt(i);
			result += string.charAt(i);
		}
		System.out.println("The results of assertEquals() Reverse test on: "+string);
		System.out.println("The reversed string: "+result);
		return result.toString();
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 *
	 */
	public String acronym(@NotNull String phrase) {
//		// TODO Write an implementation for this method declaration
		StringBuilder acronym = new StringBuilder();
		// the split looks for a whitespace
		String[] wordArray = phrase.split(" ");
		//System.out.println("The wordArray is=>: " + wordArray);
		for (String word : wordArray) {
			// explain the substring method here
			// The substring begins at the specified beginIndex and extends to the character at index endIndex - 1.
			// explain the append method here
			acronym.append(word.substring(0, 1));
		}
		System.out.println("The phrase entered is=>: " + phrase);
		System.out.println("The acronym of the phrase entered is=>: " + acronym);
		return acronym.toString();
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 */

	static class Triangle {
		// TODO Write an implementation for this method declaration
		private double sideOne;
		private double sideTwo;
		private double sideThree;
		public Triangle() {
			super();
		}
		public Triangle(double sideOne, double sideTwo, double sideThree) {
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
		}
		public double getSideOne() {
			return sideOne;
		}
		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}
		public double getSideTwo() {
			return sideTwo;
		}
		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}
		public double getSideThree() {
			return sideThree;
		}
		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral() {
			// TODO Write an implementation for this method declaration
			if (getSideOne() == getSideTwo() && getSideOne() == getSideThree()) {
				System.out.println("The triangle is Equilateral");
				return true;
			} else {
				System.out.println("The triangle is NOT Equilateral");
				return false;
			}
		}

		public boolean isIsosceles() {
			// TODO Write an implementation for this method declaration
			if ((getSideOne() == getSideTwo() && getSideOne() != getSideThree()) || (getSideOne() == getSideThree() && getSideOne() != getSideTwo()) || (getSideThree() == getSideTwo() && getSideOne() != getSideThree())) {
				//System.out.println("The triangle: "+evaluationService");
				System.out.println("The triangle IS Isosceles");
				return true;
			} else
				System.out.println("The triangle is NOT Isosceles");
			return false;
		}

		public boolean isScalene() {
			// TODO Write an implementation for this method declaration
			if ((getSideOne() != getSideTwo() && getSideOne() != getSideThree()) && getSideOne() != getSideTwo()) {
				System.out.println("The triangle is Scalene");
				return true;
			} else {
				System.out.println("The triangle is NOT Scalene");
				return false;
			}
		}
	}

	/**
	 * 4. Given a word, compute the scrabble score for that word.
	 *
	 * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
	 * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
	 * "cabbage" should be scored as worth 14 points:
	 *
	 * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
	 * point for E And to total:
	 *
	 * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
	 *
	 */

	public int isGetScrabbleScore(String string) {
		// TODO Write an implementation for this method declaration
		int score = 0;
		for (int i = 0; i < string.length(); i++) {
			if ((string.charAt(i)) == 'a' || (string.charAt(i)) == 'A') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 'b' || (string.charAt(i)) == 'B') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 4;
			}
			if ((string.charAt(i)) == 'c' || (string.charAt(i)) == 'C') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 3;
			}
			if ((string.charAt(i)) == 'd' || (string.charAt(i)) == 'D') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 2;
			}
			if ((string.charAt(i)) == 'e' || (string.charAt(i)) == 'E') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 'f' || (string.charAt(i)) == 'F') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 4;
			}
			if ((string.charAt(i)) == 'g' || (string.charAt(i)) == 'G') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 4;
			}
			if ((string.charAt(i)) == 'h' || (string.charAt(i)) == 'H') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 4;
			}
			if ((string.charAt(i)) == 'i' || (string.charAt(i)) == 'I') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 'j' || (string.charAt(i)) == 'J') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 8;
			}
			if ((string.charAt(i)) == 'k' || (string.charAt(i)) == 'K') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 5;
			}
			if ((string.charAt(i)) == 'l' || (string.charAt(i)) == 'L') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 'm' || (string.charAt(i)) == 'M') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 3;
			}
			if ((string.charAt(i)) == 'n' || (string.charAt(i)) == 'N') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 'o' || (string.charAt(i)) == 'O') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 'p' || (string.charAt(i)) == 'P') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 3;
			}
			if ((string.charAt(i)) == 'q' || (string.charAt(i)) == 'Q') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 10;
			}
			if ((string.charAt(i)) == 'r' || (string.charAt(i)) == 'R') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 's' || (string.charAt(i)) == 'S') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 't' || (string.charAt(i)) == 'T') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 'u' || (string.charAt(i)) == 'U') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 1;
			}
			if ((string.charAt(i)) == 'v' || (string.charAt(i)) == 'V') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 4;
			}
			if ((string.charAt(i)) == 'w' || (string.charAt(i)) == 'W') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 4;
			}
			if ((string.charAt(i)) == 'x' || (string.charAt(i)) == 'X') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 8;
			}
			if ((string.charAt(i)) == 'y' || (string.charAt(i)) == 'Y') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 4;
			}
			if ((string.charAt(i)) == 'z' || (string.charAt(i)) == 'Z') {
				System.out.println("string.charAt(" + i + ") = " + i + "= " + string.charAt(i));
				score = score + 10;
			}
		}
		System.out.println("\nThe Scrabble score: " + score);
		System.out.println("The played word: " + string + "\n\n");
		return score;
	}

	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 *
	 * The North American Numbering Plan (NANP) is a telephone numbering system used
	 * by many countries in North America like the United States, Canada or Bermuda.
	 * All NANP-countries share the same international country code: 1.
	 *
	 * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
	 * Area code, commonly known as area code, followed by a seven-digit local
	 * number. The first three digits of the local number represent the exchange
	 * code, followed by the unique four-digit number which is the subscriber
	 * number.
	 *
	 * The format is usually represented as
	 *
	 * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
	 * from 0 through 9.
	 *
	 * Your task is to clean up differently formatted telephone numbers by removing
	 * punctuation and the country code (1) if present.
	 *
	 * For example, the inputs
	 *
	 * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
	 * the output
	 *
	 * 6139950253
	 *
	 * Note: As this exercise only deals with telephone numbers used in
	 * NANP-countries, only 1 is considered a valid country code.
	 *
	 */

	public String cleanPhoneNumber(String string) {
		// TODO Write an implementation for this method declaration
		// Test case #1: +1 (613)-995-0253
		// assertEquals("6139950253", evaluationService.cleanPhoneNumber("+1 (613)-995-0253"));
		// Test case #2: 613-995-0253
		// assertEquals("6139950253", evaluationService.cleanPhoneNumber("613-995-0253"));
		// Test case #3: 1 613 995 0253
		// assertEquals("6139950253", evaluationService.cleanPhoneNumber("1 613 995 0253"));
		// Test case #4: 613.995.0253
		// assertEquals("6139950253", evaluationService.cleanPhoneNumber("613.995.0253"));

		String str1 = "";
		String str2 = "";
		for (int i = 0; i < string.length(); i++) {
			if ((Character.isDigit(string.charAt(i)))) {
				//System.out.println(string.charAt(i));
				str1 += string.charAt(i);
			}
		}
		if (str1.charAt(0) == '1') {
			//System.out.println(str1.charAt(0));
			for (int i = 1; i < str1.length(); i++) {
				str2 += str1.charAt(i);
			}
		} else {
			for (int i = 0; i < str1.length(); i++) {
				str2 += str1.charAt(i);
			}
		}
		System.out.println("\nfrom:\t" + string + "\nto:\t\t" + str2);
		return str2;
	}

	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 *
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 *
	 */

	//wordCount
	public String wordCount1(String string) {
		// TODO Write an implementation for this method declaration
		String word = "is";
		String[] temp = string.split(" ");
		System.out.println(string);
		//System.out.println(temp);
		int count = 0;
		for(int i = 0; i < temp.length; i++){
			if(word.equals(temp[i])){
				count++;
			}
		}
		System.out.println("The number of time the word: \"" +word+  "\" appears is: " +count+"\n");
		//return null;
		String s= String.valueOf(count);
		return  s;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 *
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 *
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 *
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 *
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 *
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 *
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 *
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 *
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 *
	 */

	public String binarySearch(int[] numbers) {
		// TODO Write an implementation for this method declaration
		System.out.println("Array:\t\t" + Arrays.toString(numbers));
		//key to be searched
		int key = 20;
		System.out.println("Key to be searched =\t" + key);
		//set first to first index
		int first = 0;
		//set last to last elements in array
		int last = numbers.length-1;
		//calculate mid of the array
		int mid = (first + last)/2;
		while( first <= last ){
			//if the mid < key, then key to be searched is in the first half of array
			if ( numbers[mid] < key ){
				first = mid + 1;
			}else if ( numbers[mid] == key ){
				//if key = element at mid, then print the location
				System.out.println("Element is found at index: " + mid+"\n");
				break;
			}else{
				//the key is to be searched in the second half of the array
				last = mid - 1;
			}
			mid = (first + last)/2;
		}
		//if first and last overlap, then key is not present in the array
		if ( first > last ){
			System.out.println("Element is not found!\n");
			return null;
		}
		String s= String.valueOf(mid);
		return  s;
	}

/**
 * 8. Create an implementation of the rotational cipher, also sometimes called
 * the Caesar cipher.
 *
 * The Caesar cipher is a simple shift cipher that relies on transposing all the
 * letters in the alphabet using an integer key between 0 and 26. Using a key of
 * 0 or 26 will always yield the same output due to modular arithmetic. The
 * letter is shifted for as many values as the value of the key.
 *
 * The general notation for rotational ciphers is ROT + <key>. The most commonly
 * used rotational cipher is ROT13.
 *
 * A ROT13 on the Latin alphabet would be as follows:
 *
 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
 * stronger than the Atbash cipher because it has 27 possible keys, and 25
 * usable keys.
 *
 * Ciphertext is written out in the same formatting as the input including
 * spaces and punctuation.
 *
 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
 * quick brown fox jumps over the lazy dog.
 */

public String rotationalCipher(String message) {
	int offset = 2;
	System.out.println("Rotational Cipher Offset: " + offset );

	StringBuilder result = new StringBuilder();
	System.out.print(result.toString());
	for (char character : message.toCharArray()) {
		if (character != ' ') {
			int originalAlphabetPosition = character - 'a';
			System.out.print("orig position: " + originalAlphabetPosition + ", ");
			int newAlphabetPosition = (originalAlphabetPosition + offset) % 26;
			System.out.print("new position: " + newAlphabetPosition);
			char newCharacter = (char) ('a' + newAlphabetPosition);
			result.append(newCharacter);
			System.out.print("\nNew Character is: "+ newCharacter);
			System.out.print("\nOrig Character is: "+ character+"\n");
		} else {
			result.append(character);
			System.out.print("\n"+ character+"\n");
		}
		System.out.print("\n");
	}
	System.out.print("The final compilation: "+ result+"\n\n");
	return result.toString();
}
	/**
	 * 9. Given a number n, determine what the nth prime is.
	 *
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 *
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 *
	 *
	 */
	public int calculateNthPrime(int n) {
		// TODO Write an implementation for this method declaration

		System.out.print("find the nth prime integer value: n = "+n);
		//reading an integer from the user
		//int n = 100;
		int num = 1, count = 0;
		int i;
		while (count < n) {
			num = num + 1;
			for (i = 2; i <= num; i++) {
				//determines the modulo and compare it with 0
				if (num % i == 0) {
					//breaks the loop if the above condition returns true
					break;
				}
			}
			if (i == num) {
				//increments the count variable by 1 if the number is prime
				count = count + 1;
			}
		}
		System.out.println("\nThe " +n +"th prime number is: " + num+"\n");
		return num;
	}

	/**
	 * 10. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 *
	 * The quick brown fox jumps over the lazy dog.
	 *
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 *
	 */
	public boolean isPangram(String string) {
		// TODO Write an implementation for this method declaration
		if (string == null) {
			return false;
		}
		Boolean[] alphabetMarker = new Boolean[26];
		Arrays.fill(alphabetMarker, false);
		int alphabetIndex = 0;
		string = string.toUpperCase();
		for (int i = 0; i < string.length(); i++) {
			if ('A' <= string.charAt(i) && string.charAt(i) <= 'Z') {
				alphabetIndex = string.charAt(i) - 'A';
				alphabetMarker[alphabetIndex] = true;
			}
		}
		for (boolean index : alphabetMarker) {
			if (!index) {
				System.out.println("The string: "+string);
				System.out.println("Does NOT contain all the letters!");
				return false;
			}
		}
		System.out.println("The string: "+string);
		System.out.println("Contains all the letters!");
		return true;
	}




	/**
	 * 11. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		// TODO Write an implementation for this method declaration
		return null;
	}

	/**
	 * 12. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		// TODO Write an implementation for this method declaration
		return false;
	}

	/**
	 * 13. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		// TODO Write an implementation for this method declaration
		return null;
	}



	/**
	 * 14 & 15. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	static class AtbashCipher {

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			// TODO Write an implementation for this method declaration
			return null;
		}

		/**
		 * Question 15
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string) {
			// TODO Write an implementation for this method declaration
			return null;
		}
	}

	/**
	 * 16. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string) {
		// TODO Write an implementation for this method declaration
		return false;
	}


	/**
	 * 17. Calculate the moment when someone has lived for 10^9 seconds.
	 * 
	 * A gigasecond is 109 (1,000,000,000) seconds.
	 * 
	 * @param given
	 * @return
	 */
	public Temporal getGigasecondDate(Temporal given) {
		// TODO Write an implementation for this method declaration
		return null;
	}

	/**
	 * 18. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param i
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int i, int[] set) {
		// TODO Write an implementation for this method declaration
		return 0;
	}

	/**
	 * 19. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string) {
		// TODO Write an implementation for this method declaration
		return false;
	}

	/**
	 * 20. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string) {
		// TODO Write an implementation for this method declaration
		return 0;
	}

}
