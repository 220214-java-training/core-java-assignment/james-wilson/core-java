/*******************************************************************
 James Wilson
 Assignment #1
 Monday, 28 February 2022
 Instructor: Carolyn Rehm

 Driver: EvaluationServiceTest.java
 Source: EvaluationService.java

 In this module, ten (10) programming problems are solved using Java as the high-level source language!!!

 Each of the ten problem descriptions is described in a heading directly before the source code for each problem.

 Program execution is accomplished through the use of the driver file!

 The entire file (all ten problems) can be run as a whole by choosing the green arrow next to class file.

 Individual problems can be run by choosing the green arrow next to each problem heading.
  ******************************************************************/

// Source Code File: EvaluationService.java
// Driver File: (below)

package com.revature.eval.java.core;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class EvaluationServiceTest {

	// Object instantiations for EvaluationServiceTest class (parent class) and Triangle class (child sub-class)
	private static final EvaluationService evaluationService = new EvaluationService();
	EvaluationService.Triangle triangle1 = new EvaluationService.Triangle(5, 4, 6);
	EvaluationService.Triangle triangle2 = new EvaluationService.Triangle(45, 45, 45);
	EvaluationService.Triangle triangle3 = new EvaluationService.Triangle(5.6, 4, 5.6);

	/*******************************************************************
	 * Question 1
	 ******************************************************************/
	@Test
	public void testReverse() {
		System.out.println("\nProblem #1 - Reverse Word\n");
		assertEquals("esrever", evaluationService.reverse("reverse"));
	}

	/*******************************************************************
	 * Question 2
	 ******************************************************************/
	@Test
	public void testAcronym() {
		System.out.println("\nProblem #2 - Acronym\n");
		assertEquals("hw", evaluationService.acronym("hello world"));
		evaluationService.acronym("this is the hello world greeting");
	}

	/*******************************************************************
	 * Question 3
	 ******************************************************************/
	@Test
	public void testEquilateralTriangle() {
		System.out.println("\nProblem #3A - Triangle test: Equilateral\n");
		assertEquals(false, triangle1.isEquilateral());
		System.out.println("triangle1 with sides: 5, 4, 6 \n");
		// Shows three (3) triangle types which do or do not match prototypes
		System.out.println("The results of three triangle test cases:");
		System.out.println("triangle1 with sides: 5, 4, 6 ");
		triangle1.isEquilateral();
		System.out.println("triangle2 with sides: 45, 45, 45");
		triangle2.isEquilateral();
		System.out.println("triangle3 with sides: 5.6, 4, 5.6 ");
		triangle3.isEquilateral();
	}

	@Test
	public void testIsoscelesTriangle() {
		System.out.println("\nProblem #3B - Triangle test: Isosceles\n");
		System.out.println("triangle1 with sides: 5, 4, 6\n");
		assertEquals(false, triangle1.isIsosceles());
		// Shows three (3) triangle types which do or do not match prototypes
		System.out.println("The results of three triangle test cases:");
		System.out.println("triangle1 with sides: 5, 4, 6 ");
		triangle1.isIsosceles();
		System.out.println("triangle2 with sides: 45, 45, 45");
		triangle2.isIsosceles();
		System.out.println("triangle3 with sides: 5.6, 4, 5.6");
		triangle3.isIsosceles();
	}

	@Test
	public void testScaleneTriangle() {
		System.out.println("\nProblem #3C - Triangle test: Scalene\n");
		System.out.println("triangle1 with sides: 5, 4, 6 \n");
		assertEquals(true, triangle1.isScalene());
		// Shows three (3) triangle types which do or do not match prototypes
		System.out.println("The results of three triangle test cases:");
		System.out.println("triangle1 with sides: 5, 4, 6 ");
		triangle1.isScalene();
		System.out.println("triangle2 with sides: 45, 45, 45");
		triangle2.isScalene();
		System.out.println("triangle3 with sides: 5.6, 4, 5.6");
		triangle3.isScalene();
	}

	/*******************************************************************
	 * Question 4
	 ******************************************************************/
	@Test
	public void testGetScrabbleScore() {
		System.out.println("\nProblem #4 - Scrabble Score\n");
		assertEquals(7, evaluationService.isGetScrabbleScore("dog"));
		evaluationService.isGetScrabbleScore("zebra");
	}

	/*******************************************************************
	 * Question 5
	 ******************************************************************/
	@Test
	public void testCleanPhoneNumber() {
		System.out.println("\nProblem #5 - Clean Phone Number");
		// Test case #1: +1 (613)-995-0253
		assertEquals("6139950253", evaluationService.cleanPhoneNumber("+1 (613)-995-0253"));
		// Test case #2: 613-995-0253
		assertEquals("6139950253", evaluationService.cleanPhoneNumber("613-995-0253"));
		// Test case #3: 1 613 995 0253
		assertEquals("6139950253", evaluationService.cleanPhoneNumber("1 613 995 0253"));
		// Test case #4: 613.995.0253
		assertEquals("6139950253", evaluationService.cleanPhoneNumber("613.995.0253"));
	}

	/*******************************************************************
	 * Question 6
	 ******************************************************************/
	@Test
	public void testWordCount() {
		// Test case #1:
		System.out.println("\nProblem #6 - Word Count\n");
		System.out.println("Test case #1:");
		assertEquals("2", evaluationService.wordCount1("Everything is beautiful as it is again"));
		// Test case #2:
		System.out.println("Test case #2:");
		assertEquals("2", evaluationService.wordCount1("Is everything as beautiful as is possible again, or is it not?"));
		// Test case #3:
		System.out.println("Test case #3:");
		assertEquals("2", evaluationService.wordCount1("...is everything as beautiful as is possible again, or is it not?"));
		// Test case #4:
		System.out.println("Test case #4:");
		assertEquals("3", evaluationService.wordCount1(" is everything as beautiful as is possible again, or is it not?"));
	}

	/*******************************************************************
	 * Question 7
	 ******************************************************************/
	@Test
	public void testBinarySearch() {
		System.out.println("\nProblem #7 - Binary Search\n");
		// Test case #1 (pre-sorted array with key present):
		//		Key = 20
		//		intArray = {5, 10, 15, 20, 25, 30, 35}
		//		index =     0,  1,  2,  3,  4,  5,  6
		//		expected:              "3"
		int[] intArray = new int[]{5,10,15,20,25,30,35};
		System.out.println("Test case #1 - pre-sorted array with key present:");
		assertEquals("3", evaluationService.binarySearch(intArray));
		//System.out.println("\n");
		// Test case #1 (pre-sorted array):
		//		Key = 20
		//		intArray = {5, 10, 15, 21, 25, 30, 35}
		//		index =     0,  1,  2,  3,  4,  5,  6
		//		expected:   null
		int[] intArray2 = new int[]{5,10,15,21,25,30,35};
		System.out.println("Test case #2 - pre-sorted array with key NOT present:");
		assertEquals(null, evaluationService.binarySearch(intArray2));
	}

	/*******************************************************************
	 * Question 8
	 ******************************************************************/
	@Test
	public void testRotationalCipher() {
		System.out.println("\nProblem #8 - Rotational Cipher\n");
		// Test case #1:
		System.out.println("Test case #1:");
		assertEquals("ufhufh", evaluationService.rotationalCipher("sdfsdf"));
		// Test case #2:
		System.out.println("\nTest case #2:");
		assertEquals("e f z", evaluationService.rotationalCipher("c d x"));
		//assertEquals("M f z", evaluationService.rotationalCipher("1 d x"));
	}

	/*******************************************************************
	 * Question 9
	 ******************************************************************/
	@Test
	public void testCalculateNthPrime() {
		System.out.println("\nProblem #9 - Calculate nth Prime\n");
		// Test case #1:
		System.out.println("Test case #1: n = 100 => find the 100th prime number");
		assertEquals(541, evaluationService.calculateNthPrime(100));
		// Test case #2:
		System.out.println("Test case #2: n = 100 => find the 100th prime number");
		assertEquals(29, evaluationService.calculateNthPrime(10));
	}

	/*******************************************************************
	 * Question 10
	 ******************************************************************/
	@Test
	public void testIsPangram() {
		System.out.println("\nProblem #10 - Panagram\n");
		// Test case #1: Valid Case - abcdefghijklmnopqrstuvwxyz
		System.out.println("Test case #1: ");
		assertEquals(true, evaluationService.isPangram("abcdefghijklmnopqrstuvwxyz"));
		// Test case #2: Invalid Case - abcdefghlmnopqrstuvwxyz
		System.out.println("\nTest case #2: ");
		assertEquals(false, evaluationService.isPangram("abcdefghlmnopqrstuvwxyz"));
//		The quick brown fox jumps over the lazy dog
		// Test case #3: Valid Case - The quick brown fox jumps over the lazy dog
		System.out.println("\nTest case #3: ");
		assertEquals(true, evaluationService.isPangram("The quick brown fox jumps over the lazy dog"));

	}
}